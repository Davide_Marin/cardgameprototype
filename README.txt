OVERVIEW PROTOTIPO 

Cliccate su Add Card in alto a sinistra

1)Ho aggiunto un actor generico (si è una panchina di unreal) che funziona da "punching bag" per testare le carte

2)Le carte possono essere spostate tra di loro, per giocarne una basta trascinarla sopra la panchina

3)I dadi sono trascinabili e si posizionano nella runa in cui li droppi, si possono togliere con tasto destro e sinistro del mouse (per test ho anche aggiunto che si possono mettere senza trascinarle con un doppio click, attenzione che se si fa cosi non si vede il feedback del dado che va sopra la runa)

4)Le carte hanno un costo e quando una carta viene giocata il numero di rune visualizzate sarà il costo della carta giocata (per ora ne ho messe al massimo è 3 per aumentarlo bisogna aggiungere uno slot runa nella UI)

5)Le carte vengono generate prese da una tabella. Nella tabella si definiscono le proprietà delle carte (descrizione, costo, immagine ecc..)

6)Per aggiungere una o piu proprietà a tutte le carte basta modificare la struttura S card info che contiene le caratteristiche di una carta (descrizione, costo, immagine ecc..) nella tabella di "popolano" e nella struttura si "definiscono"



NOTE

Scrivendo questo read me mi sono ricordato che manca la quantity dei dadi quando li usi.
Avevo fatto una variabile ma mi sono dimenticato di implementarla nella logica e ora purtroppo non ho piu tempo di farlo ma non penso sia troppo complicato da fare

Ci saranno bug ovviamente, i piu noti saranno sicuramente la sminchiatura dei widgen nella UI non allineati bene ecc... (forgive me)

Mi sarebbe piacuto fare anche un puntatore esterno invece che trascinare tutta la carta ma per il tempo che avevo ho lasciato perdere e mi sono concentrato su altre funzionalità piu importanti

Se potessi rifarei tutto da capo visto che ho messo in piedi delle logiche non proprio p u l i t i s s i m e

Tempo dedicato: da venerdi sera a domenica pomeriggio


